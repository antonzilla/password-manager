#ifndef ITEMSERVICE_H
#define ITEMSERVICE_H

#include "item.h"
#include <QString>
#include <QList>
#include <QSqlDatabase>
#include <QSharedPointer>

class ItemService
{
public:
    static QSharedPointer<ItemService> getInstance();

    ItemService(ItemService&) = delete;
    virtual ~ItemService() = default;
    void operator=(const ItemService&) = delete;

    void setDatabase(const QSqlDatabase &db);
    void resetDatabase();
    QList<Item> getItems(const QString &key);
    void createItem(const Item &item, const QString &key);
    void updateItem(const Item &item, const QString &key);
    void deleteItem(const QUuid &itemId);

private:
    static QSharedPointer<ItemService> instance;
    QSqlDatabase m_db;

    ItemService() = default;
};

#endif // ITEMSERVICE_H
