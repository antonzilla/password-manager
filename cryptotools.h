#ifndef CRYPTOTOOLS_H
#define CRYPTOTOOLS_H

#include <QString>

QString deriveMasterPassword(const QString &password, const QString &salt, uint iterations);

QString generateSalt();

QString generateIv();

QString encrypt(const QString &plain, const QString &key, const QString &iv);

QString decrypt(const QString &chiper, const QString &key, const QString &iv);

#endif // CRYPTOTOOLS_H
