#include "itemservice.h"
#include "authservice.h"
#include "exceptions.h"
#include "mainwindow.h"
#include "loginwidget.h"
#include "cryptotools.h"
#include <QStringList>
#include <QStatusBar>
#include <QMessageBox>
#include <QSqlQuery>


#include <QDebug>

bool seed(QSqlDatabase &db)
{
    QStringList queries;

    queries.append(
        "CREATE TABLE IF NOT EXISTS key_info ("
        "   salt TEXT NOT NULL,"
        "   iterations INTEGER NOT NULL,"
        "   test_chiper TEXT NOT NULL,"
        "   test_iv TEXT NOT NULL"
        ")"
    );

    queries.append(
        "CREATE TABLE IF NOT EXISTS items ("
        "   id VARCHAR(36) NOT NULL PRIMARY KEY,"
        "   name TEXT NOT NULL,"
        "   login TEXT NOT NULL,"
        "   password_chiper TEXT NOT NULL,"
        "   iv TEXT NOT NULL,"
        "   'order' INTEGER NOT NULL"
        ")"
    );

    db.transaction();

    for (auto it = queries.cbegin(); it != queries.cend(); ++it)
    {
        QSqlQuery query(db);
        if (!query.exec(*it))
        {
            db.rollback();
            return false;
        }
    }

    db.commit();

    return true;
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setUpUi();
}

MainWindow::~MainWindow()
{
    auto itemService = ItemService::getInstance();
    auto authService = AuthService::getInstance();
    itemService->resetDatabase();
    authService->resetDatabase();
    if (m_connectionName.size() > 0)
    {
        QSqlDatabase::removeDatabase(m_connectionName);
    }
}

void MainWindow::addItem(const Item &item)
{
    try
    {
        auto itemService = ItemService::getInstance();
        itemService->createItem(item, m_key);
        m_listWidget->setItems(itemService->getItems(m_key));
        m_listWidget->setActiveItem(item.id);
    }
    catch (ServiceException)
    {
        showErrorDialog("Failed to perform operation");
    }
}

void MainWindow::updateItem(const Item &item)
{
    try
    {
        auto itemService = ItemService::getInstance();
        itemService->updateItem(item, m_key);
        m_listWidget->setItems(itemService->getItems(m_key));
        m_listWidget->setActiveItem(item.id);
    }
    catch (ServiceException)
    {
        showErrorDialog("Failed to perform operation");
    }
}

void MainWindow::deleteItem(const Item &item)
{
    try
    {
        auto itemService = ItemService::getInstance();
        itemService->deleteItem(item.id);
        m_listWidget->setItems(itemService->getItems(m_key));
    }
    catch (ServiceException)
    {
        showErrorDialog("Failed to perform operation");
    }
}

void MainWindow::unlock(const QString &masterPassword, const QString &dbFilePath)
{

    auto itemService = ItemService::getInstance();
    auto authService = AuthService::getInstance();
    itemService->resetDatabase();
    authService->resetDatabase();

    if (m_connectionName.size() > 0)
    {
        QSqlDatabase::removeDatabase(m_connectionName);
    }

    bool newDb = false;
    auto db = QSqlDatabase::addDatabase("QSQLITE");
    if (dbFilePath.size() == 0)
    {
        QString dbName = QDir(QDir::currentPath()).absoluteFilePath("db.sqlite3");
        newDb = !QFile::exists(dbName);
        db.setDatabaseName(dbName);
    }
    else
    {
        db.setDatabaseName(dbFilePath);
    }

    db.open();
    m_connectionName = db.connectionName();
    seed(db);
    itemService->setDatabase(db);
    authService->setDatabase(db);

    try
    {
        if (newDb)
        {
            QString salt = generateSalt();
            m_key = deriveMasterPassword(masterPassword, salt, 1000);
            authService->saveKeyInfo(masterPassword, salt, 1000);
        }
        else
        {
            KeyInfo keyInfo = authService->loadKeyInfo();

            if (!authService->checkMasterPassword(masterPassword, keyInfo))
            {
                showErrorDialog("Invalid master password");
                return;
            }

            m_key = deriveMasterPassword(masterPassword, keyInfo.salt, keyInfo.iterations);
        }
    }
    catch (ServiceException)
    {
        showErrorDialog("Login failed");
        return;
    }

    try
    {
        m_listWidget->setItems(itemService->getItems(m_key));
        m_stackedLayout->setCurrentIndex(1);
    }
    catch (ServiceException)
    {
        showErrorDialog("Failed to load data");
    }
}

void MainWindow::setUpUi()
{
    setWindowTitle("Password manager");
    setStatusBar(new QStatusBar());

    m_listWidget = new ListWidget();
    auto loginWidget = new LoginWidget();

    m_stackedLayout = new QStackedLayout();
    m_stackedLayout->addWidget(loginWidget);
    m_stackedLayout->addWidget(m_listWidget);
    m_stackedLayout->setCurrentIndex(0);

    auto w = new QWidget();
    w->setContentsMargins(10, 10, 10, 10);
    w->setLayout(m_stackedLayout);

    connect(loginWidget, &LoginWidget::unlocked, this, &MainWindow::unlock);
    connect(m_listWidget, &ListWidget::passwordCopied, this, [&] () {statusBar()->showMessage("Password copied", 4000);});
    connect(m_listWidget, &ListWidget::loginCopied, this, [&] () {statusBar()->showMessage("Login copied", 4000);});
    connect(m_listWidget, &ListWidget::added, this, &MainWindow::addItem);
    connect(m_listWidget, &ListWidget::updated, this, &MainWindow::updateItem);
    connect(m_listWidget, &ListWidget::deleted, this, &MainWindow::deleteItem);

    setCentralWidget(w);
}

void MainWindow::showErrorDialog(const QString &errorText)
{
    auto d = new QMessageBox(this);
    d->setIcon(QMessageBox::Critical);
    d->setWindowTitle("Error");
    d->setText(errorText);
    d->setAttribute(Qt::WA_DeleteOnClose);
    d->exec();
}
