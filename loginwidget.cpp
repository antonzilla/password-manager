#include "loginwidget.h"
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QDebug>

LoginWidget::LoginWidget(QWidget *parent) : QWidget(parent)
{
    setUpUi();
}

void LoginWidget::setUpUi()
{
    m_fileDialog = new QFileDialog(this);
    m_fileDialog->setViewMode(QFileDialog::List);
    m_fileDialog->setFileMode(QFileDialog::ExistingFile);

    m_passwordInput = new QLineEdit();
    m_passwordInput->setEchoMode(QLineEdit::Password);
    m_passwordInput->setPlaceholderText("Enter master password");

    m_dbFileInput = new QLineEdit();
    m_dbFileInput->setReadOnly(true);
    m_dbFileInput->setPlaceholderText("Select db file");
    auto selectDbFileBtn = new QPushButton("&Select");
    auto clearDbFileBtn = new QPushButton("&Clear");

    auto unlockBtn = new QPushButton("&Unlock");

    auto formLayout = new QGridLayout();
    formLayout->setVerticalSpacing(20);
    formLayout->addWidget(new QLabel("Login"), 0, 0, 1, 3, Qt::AlignCenter);
    formLayout->addWidget(m_passwordInput, 1, 0, 1, 3);
    formLayout->addWidget(m_dbFileInput, 2, 0);
    formLayout->addWidget(clearDbFileBtn, 2, 1);
    formLayout->addWidget(selectDbFileBtn, 2, 2);
    formLayout->addWidget(unlockBtn, 3, 0, 1, 3, Qt::AlignCenter);

    auto mainLayout = new QHBoxLayout();
    mainLayout->setContentsMargins(0, 0, 0, 0);
    auto subLyaout = new QVBoxLayout();
    subLyaout->addStretch();
    subLyaout->addLayout(formLayout);
    subLyaout->setAlignment(formLayout, Qt::AlignCenter);
    subLyaout->addStretch();
    mainLayout->addStretch();
    mainLayout->addLayout(subLyaout);
    mainLayout->addStretch();

    connect(selectDbFileBtn, &QPushButton::clicked, this, &LoginWidget::openFileDialog);
    connect(clearDbFileBtn, &QPushButton::clicked, this, [&] () {m_dbFileInput->setText("");});
    connect(unlockBtn, &QPushButton::clicked, this, &LoginWidget::unlock);

    setLayout(mainLayout);
}

void LoginWidget::openFileDialog()
{
    if (m_fileDialog->exec())
    {
        auto files = m_fileDialog->selectedFiles();
        m_dbFileInput->setText(files.first());
        m_dbFileInput->setCursorPosition(0);
    }
}

void LoginWidget::unlock()
{
    QString password = m_passwordInput->text();
    if (password.size() == 0)
    {
        return;
    }
    emit unlocked(password, m_dbFileInput->text());
}
