#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include "itemwidget.h"
#include <QList>
#include <QUuid>
#include <QWidget>
#include <QListWidget>
#include <QStackedLayout>

class ListWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ListWidget(QWidget *parent = nullptr);

public slots:
    void setItems(const QList<Item> &items);
    void setActiveItem(const QUuid &itemId);

private slots:
    void activateListItem(QListWidgetItem *listItem);
    void openItemDialog(bool newItem = true);
    void closeItemDialog(const Item &item, bool newItem = true);

signals:
    void passwordCopied();
    void loginCopied();
    void deleted(Item item);
    void added(Item item);
    void updated(Item item);

private:
    QListWidget *m_listWidget;
    ItemWidget *m_itemWidget;
    QStackedLayout *m_stackedLayout;
    QListWidgetItem *m_currentListItem;

    static QListWidgetItem *buildListItem(const Item &item);
    static QListWidgetItem *bindDataToListItem(QListWidgetItem *listItem, const Item &item);

    void setUpUi();
    void showItemWidget(const Item &item);
    void hideItemWidget();
};

#endif // LISTWIDGET_H
