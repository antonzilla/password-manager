#ifndef AUTHSERVICE_H
#define AUTHSERVICE_H

#include "keyinfo.h"
#include <QString>
#include <QSqlDatabase>
#include <QSharedPointer>

class AuthService
{
public:
    static QSharedPointer<AuthService> getInstance();

    AuthService(AuthService&) = delete;
    virtual ~AuthService() = default;
    void operator=(const AuthService&) = delete;

    void setDatabase(const QSqlDatabase &db);
    void resetDatabase();
    KeyInfo saveKeyInfo(const QString &password, const QString &salt, int iterations);
    bool checkMasterPassword(const QString &password, const KeyInfo &keyInfo);
    KeyInfo loadKeyInfo();

private:
    static QSharedPointer<AuthService> instance;
    QSqlDatabase m_db;

    AuthService() = default;
};

#endif // AUTHSERVICE_H
