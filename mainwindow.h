#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "listwidget.h"
#include <QString>
#include <QMainWindow>
#include <QStackedLayout>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow();

private slots:
    void addItem(const Item &item);
    void updateItem(const Item &item);
    void deleteItem(const Item &item);
    void unlock(const QString &masterPassword, const QString &dbFilePath);

private:
    ListWidget *m_listWidget;
    QStackedLayout *m_stackedLayout;
    QString m_connectionName;
    QString m_key;

    void setUpUi();
    void showErrorDialog(const QString &errorText);
};
#endif // MAINWINDOW_H
