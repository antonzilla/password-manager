#ifndef ITEMDIALOG_H
#define ITEMDIALOG_H

#include "item.h"
#include <QUuid>
#include <QDialog>
#include <QLineEdit>

class ItemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ItemDialog(QWidget *parent = nullptr);
    void setItem(const Item &item);

signals:
    void saved(Item item);

private slots:
    void save();

private:
    QUuid m_itemId;
    QLineEdit *m_nameInput;
    QLineEdit *m_loginInput;
    QLineEdit *m_passwordInput;


    void setUpUi();
};

#endif // ITEMDIALOG_H
