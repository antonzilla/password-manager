#include "listwidget.h"
#include "itemdialog.h"
#include "placeholderwidget.h"
#include <QGridLayout>
#include <QPushButton>

ListWidget::ListWidget(QWidget *parent) : QWidget(parent), m_currentListItem(nullptr)
{
    setUpUi();
}

void ListWidget::setItems(const QList<Item> &items)
{
    m_listWidget->clear();
    m_currentListItem = nullptr;
    hideItemWidget();
    for (auto it = items.cbegin(); it != items.cend(); ++it)
    {
        QListWidgetItem *listItem = ListWidget::buildListItem(*it);
        m_listWidget->addItem(listItem);
    }
}

void ListWidget::setActiveItem(const QUuid &itemId)
{
    for (int i = 0; i < m_listWidget->count(); ++i)
    {
        QListWidgetItem *listItem = m_listWidget->item(i);
        Item item = listItem->data(Qt::UserRole).value<Item>();
        if (item.id == itemId)
        {
            activateListItem(listItem);
            break;
        }
    }
}

void ListWidget::activateListItem(QListWidgetItem *listItem)
{
    m_currentListItem = listItem;
    m_listWidget->setCurrentItem(listItem);
    showItemWidget(listItem->data(Qt::UserRole).value<Item>());
}

void ListWidget::setUpUi()
{
    m_listWidget = new QListWidget();
    m_itemWidget = new ItemWidget();

    m_stackedLayout = new QStackedLayout();
    m_stackedLayout->addWidget(new PlaceholderWidget("Nothing to show"));
    m_stackedLayout->addWidget(m_itemWidget);

    auto addBtn = new QPushButton("&Add");

    auto mainLayout = new QGridLayout();
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(addBtn, 0, 0, Qt::AlignRight);
    mainLayout->addWidget(m_listWidget, 1, 0);
    mainLayout->addLayout(m_stackedLayout, 1, 1);

    mainLayout->setColumnStretch(0, 1);
    mainLayout->setColumnStretch(1, 2);

    connect(m_listWidget, &QListWidget::itemActivated, this, &ListWidget::activateListItem);
    connect(m_itemWidget, &ItemWidget::deleted, this, [&] () {emit deleted(m_currentListItem->data(Qt::UserRole).value<Item>());});
    connect(m_itemWidget, &ItemWidget::updated, this, [&] () {openItemDialog(false);});
    connect(m_itemWidget, &ItemWidget::passwordCopied, this, [&] () {emit passwordCopied();});
    connect(m_itemWidget, &ItemWidget::loginCopied, this, [&] () {emit loginCopied();});
    connect(addBtn, &QPushButton::clicked, this, [&] () {openItemDialog(true);});

    setLayout(mainLayout);
}

void ListWidget::showItemWidget(const Item &item)
{
    m_stackedLayout->setCurrentIndex(1);
    m_itemWidget->setItem(item);
}

void ListWidget::hideItemWidget()
{
    m_stackedLayout->setCurrentIndex(0);
    m_itemWidget->clear();
}

void ListWidget::openItemDialog(bool newItem)
{
    auto d = new ItemDialog(this);
    d->setAttribute(Qt::WA_DeleteOnClose);
    d->setModal(true);
    d->setWindowTitle(newItem ? "Add new item" : "Edit item");
    if (newItem)
    {
        connect(d, &ItemDialog::saved, this, [&] (Item item) {closeItemDialog(item, true);});
    }
    else
    {
        Item item = m_currentListItem->data(Qt::UserRole).value<Item>();
        d->setItem(item);
        connect(d, &ItemDialog::saved, this, [&] (Item item) {closeItemDialog(item, false);});
    }
    d->exec();
}

void ListWidget::closeItemDialog(const Item &item, bool newItem)
{
    if (newItem)
    {
        emit added(item);
    }
    else
    {
        emit updated(item);
    }
}

QListWidgetItem *ListWidget::buildListItem(const Item &item)
{
    auto listItem = new QListWidgetItem(item.name);
    bindDataToListItem(listItem, item);
    return listItem;
}

QListWidgetItem *ListWidget::bindDataToListItem(QListWidgetItem *listItem, const Item &item)
{
    auto data = QVariant::fromValue(item);
    listItem->setData(Qt::UserRole, data);
    return listItem;
}
