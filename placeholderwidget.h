#ifndef PLACEHOLDERWIDGET_H
#define PLACEHOLDERWIDGET_H

#include <QString>
#include <QWidget>

class PlaceholderWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlaceholderWidget(const QString &placeholder, QWidget *parent = nullptr);
    void setPlaceholderText(const QString &text);
    void paintEvent(QPaintEvent *e) override;

private:
    QString m_placeholder;
};

#endif // PLACEHOLDERWIDGET_H
