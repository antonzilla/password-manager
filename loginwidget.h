#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include <QString>
#include <QLineEdit>
#include <QFileDialog>

class LoginWidget : public QWidget
{

    Q_OBJECT

public:
    explicit LoginWidget(QWidget *parent = nullptr);

signals:
    void unlocked(QString password, QString dbFile);

private slots:
    void openFileDialog();
    void unlock();

private:
    QLineEdit *m_passwordInput;
    QLineEdit *m_dbFileInput;
    QFileDialog *m_fileDialog;

    void setUpUi();
};

#endif // LOGINWIDGET_H
