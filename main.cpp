#include "mainwindow.h"
#include <QApplication>
#include <QScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    auto screenGeomerty = QApplication::screens().at(0)->geometry();
    w.resize(screenGeomerty.width() * 0.5, screenGeomerty.height() * 0.5);
    w.move(screenGeomerty.center() - w.frameGeometry().center());
    w.show();
    return a.exec();
}
