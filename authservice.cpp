#include "cryptotools.h"
#include "exceptions.h"
#include "authservice.h"
#include <QVariant>
#include <QSqlQuery>

#define TEST_PHRASE "test"

QSharedPointer<AuthService> AuthService::instance = nullptr;

QSharedPointer<AuthService> AuthService::getInstance()
{
    if (instance == nullptr)
    {
        instance = QSharedPointer<AuthService>(new AuthService());
    }
    return instance;
}

void AuthService::setDatabase(const QSqlDatabase &db)
{
    m_db = db;
}

void AuthService::resetDatabase()
{
    m_db = QSqlDatabase();
}

KeyInfo AuthService::saveKeyInfo(const QString &password, const QString &salt, int iterations)
{
    QString key = deriveMasterPassword(password, salt, iterations);
    QString testIv = generateIv();
    QString testChiper = encrypt(TEST_PHRASE, key, testIv);

    m_db.transaction();

    QSqlQuery query(m_db);

    if (!query.exec("DELETE FROM key_info WHERE 1"))
    {
        m_db.rollback();
        throw ServiceException();
    }

    query.prepare(
        "INSERT INTO key_info (salt, iterations, test_chiper, test_iv) VALUES(:salt, :iterations, :test_chiper, :test_iv)"
    );
    query.bindValue(":salt", salt);
    query.bindValue(":iterations", iterations);
    query.bindValue(":test_chiper", testChiper);
    query.bindValue(":test_iv", testIv);

    if (query.exec())
    {
        m_db.commit();
        return KeyInfo(salt, iterations, testChiper, testIv);
    }

    m_db.rollback();
    throw ServiceException();
}

bool AuthService::checkMasterPassword(const QString &password, const KeyInfo &keyInfo)
{
    QString key = deriveMasterPassword(password, keyInfo.salt, keyInfo.iterations);
    return decrypt(keyInfo.testChiper, key, keyInfo.testIv) == TEST_PHRASE;
}

KeyInfo AuthService::loadKeyInfo()
{
    QSqlQuery query(m_db);
    if (!query.exec("SELECT * FROM key_info LIMIT 1"))
    {
        throw ServiceException();
    }

    if (!query.first())
    {
        throw ServiceException();
    }

    QString salt = query.value(0).toString();
    int iterations = query.value(1).toInt();
    QString testChiper = query.value(2).toString();
    QString testIv = query.value(3).toString();

    return KeyInfo(salt, iterations, testChiper, testIv);
}
