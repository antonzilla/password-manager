#include "cryptotools.h"
#include "exceptions.h"
#include "itemservice.h"
#include <QVariant>
#include <QSqlQuery>
#include <QSqlError>

#include <QDebug>

QSharedPointer<ItemService> ItemService::instance = nullptr;

QSharedPointer<ItemService> ItemService::getInstance()
{
    if (instance == nullptr)
    {
        instance = QSharedPointer<ItemService>(new ItemService());
    }
    return instance;
}

void ItemService::setDatabase(const QSqlDatabase &db)
{
    m_db = db;
}

void ItemService::resetDatabase()
{
    m_db = QSqlDatabase();
}

QList<Item> ItemService::getItems(const QString &key)
{
    QSqlQuery query("SELECT * FROM items ORDER BY 'order' ASC", m_db);
    if (query.lastError().isValid())
    {
        qDebug() << query.lastError().text();
        throw ServiceException();
    }
    QList<Item> result;
    while (query.next())
    {
        QString passwordChiper = query.value(3).toString();
        QString iv = query.value(4).toString();
        QString password = decrypt(passwordChiper, key, iv);
        result.append(Item(query.value(0).toString(), query.value(1).toString(), query.value(2).toString(), password));
    }
    return result;
}

void ItemService::createItem(const Item &item, const QString &key)
{
    QString iv = generateIv();
    QSqlQuery query(m_db);

    if (!query.exec("SELECT COUNT(id) FROM items"))
    {
        throw ServiceException();
    }

    if (!query.first())
    {
        throw ServiceException();
    }

    int count = query.value(0).toInt();

    query.prepare(
        "INSERT INTO items (id, name, login, password_chiper, iv, 'order') VALUES(:id, :name, :login, :password_chiper, :iv, :order)"
    );
    query.bindValue(":id", item.id.toString(QUuid::WithoutBraces));
    query.bindValue(":name", item.name);
    query.bindValue(":login", item.login);
    query.bindValue(":password_chiper", encrypt(item.password, key, iv));
    query.bindValue(":iv", iv);
    query.bindValue(":order", count);
    if (!query.exec())
    {
        qDebug() << query.lastError().text();
        throw ServiceException();
    }
}

void ItemService::updateItem(const Item &item, const QString &key)
{
    QString iv = generateIv();
    QSqlQuery query(m_db);
    query.prepare(
        "UPDATE items SET name = :name, login = :login, password_chiper = :password_chiper, iv = :iv WHERE id = :id"
    );
    query.bindValue(":id", item.id.toString(QUuid::WithoutBraces));
    query.bindValue(":name", item.name);
    query.bindValue(":login", item.login);
    query.bindValue(":password_chiper", encrypt(item.password, key, iv));
    query.bindValue(":iv", iv);
    if (!query.exec())
    {
        throw ServiceException();
    }
}

void ItemService::deleteItem(const QUuid &itemId)
{
    QSqlQuery query(m_db);
    query.prepare(
        "DELETE FROM items WHERE id = :id"
    );
    query.bindValue(":id", itemId.toString(QUuid::WithoutBraces));
    if (!query.exec())
    {
        throw ServiceException();
    }
}
