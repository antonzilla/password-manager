#ifndef ITEM_H
#define ITEM_H

#include <QString>
#include <QUuid>
#include <QMetaType>

struct Item
{
    QUuid id;
    QString name;
    QString login;
    QString password;

    Item()
        : id(QUuid::createUuid()), name(""), login(""), password("")
    {
    }

    Item(const QString &name, const QString &login, const QString &password)
        : id(QUuid::createUuid()), name(name.trimmed()), login(login.trimmed()), password(password)
    {
    }

    Item(const QUuid &id, const QString &name, const QString &login, const QString &password)
        : id(id), name(name.trimmed()), login(login.trimmed()), password(password)
    {
    }
};

Q_DECLARE_METATYPE(Item);

#endif // ITEM_H
