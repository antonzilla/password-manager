#include "itemwidget.h"
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QApplication>
#include <QClipboard>

ItemWidget::ItemWidget(QWidget *parent) : QWidget(parent), m_itemId(QUuid())
{
    setUpUi();
}

Item ItemWidget::getItem() const
{
    return Item(m_itemId, m_nameLabel->text(), m_loginInput->text(), m_passwordInput->text());
}

void ItemWidget::setItem(const Item &item)
{
    m_nameLabel->setText(item.name);
    m_loginInput->setText(item.login);
    m_passwordInput->setText(item.password);
    m_itemId = item.id;
}

void ItemWidget::clear()
{
    m_nameLabel->setText("");
    m_loginInput->setText("");
    m_passwordInput->setText("");
    m_itemId = QUuid();
}

void ItemWidget::editItem()
{
    emit updated(getItem());
}

void ItemWidget::deleteItem()
{
    emit deleted();
}

void ItemWidget::copyPassword()
{
    QApplication::clipboard()->setText(m_passwordInput->text());
    emit passwordCopied();
}

void ItemWidget::copyLogin()
{
    QApplication::clipboard()->setText(m_loginInput->text());
    emit loginCopied();
}

void ItemWidget::setUpUi()
{
    auto mainLayout = new QVBoxLayout();
    mainLayout->setContentsMargins(0, 0, 0, 0);

    m_nameLabel = new QLabel();
    m_loginInput = new QLineEdit();
    m_loginInput->setReadOnly(true);
    m_passwordInput = new QLineEdit();
    m_passwordInput->setEchoMode(QLineEdit::Password);
    m_passwordInput->setReadOnly(true);
    auto passwordCopyBtn = new QPushButton("&Copy");
    auto loginCopyBtn = new QPushButton("&Copy");
    auto editBtn = new QPushButton("&Edit");
    auto deleteBtn = new QPushButton("&Delete");

    auto buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(editBtn);
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(deleteBtn);

    auto formLayout = new QGridLayout();
    formLayout->addWidget(m_nameLabel, 0, 0, 1, 3);
    formLayout->addWidget(m_loginInput, 1, 0, 1, 2);
    formLayout->addWidget(loginCopyBtn, 1, 2);
    formLayout->addWidget(m_passwordInput, 2, 0, 1, 2);
    formLayout->addWidget(passwordCopyBtn, 2, 2);
    formLayout->setColumnStretch(0, 2);

    mainLayout->addLayout(formLayout);
    mainLayout->addStretch();
    mainLayout->addLayout(buttonsLayout);

    connect(editBtn, &QPushButton::clicked, this, &ItemWidget::editItem);
    connect(deleteBtn, &QPushButton::clicked, this, &ItemWidget::deleteItem);
    connect(passwordCopyBtn, &QPushButton::clicked, this, &ItemWidget::copyPassword);
    connect(loginCopyBtn, &QPushButton::clicked, this, &ItemWidget::copyLogin);

    setLayout(mainLayout);
}
