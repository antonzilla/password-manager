#include "cryptotools.h"
#include <string>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/secblock.h>
#include <cryptopp/sha.h>
#include <cryptopp/pwdbased.h>
#include <cryptopp/hex.h>
#include <cryptopp/osrng.h>
#include <QByteArray>

using namespace std;
using namespace CryptoPP;

QString deriveMasterPassword(const QString &password, const QString &salt, uint iterations)
{
    QByteArray passwordBytes = password.toUtf8();
    QByteArray saltBytes = salt.toUtf8();
    SecByteBlock result(AES::DEFAULT_KEYLENGTH);
    PKCS5_PBKDF2_HMAC<SHA256> pbkdf;
    pbkdf.DeriveKey(
        result, result.size(),
        0x00,
        reinterpret_cast<const byte*>(passwordBytes.data()), passwordBytes.size(),
        reinterpret_cast<const byte*>(saltBytes.data()), saltBytes.size(),
        iterations
    );
    string hexResult;
    ArraySource(result, result.size(), true, new HexEncoder(new StringSink(hexResult)));
    return QString::fromStdString(hexResult);
}

QString generateSalt()
{
    SecByteBlock scratch(16);

    AutoSeededRandomPool rng;
    rng.GenerateBlock(scratch, scratch.size());

    string salt;
    HexEncoder hex(new StringSink(salt));
    hex.Put(scratch, scratch.size());
    hex.MessageEnd();

    return QString::fromStdString(salt);
}

QString generateIv()
{
    SecByteBlock scratch(AES::BLOCKSIZE);

    AutoSeededRandomPool rng;
    rng.GenerateBlock(scratch, scratch.size());

    string iv;
    HexEncoder hex(new StringSink(iv));
    hex.Put(scratch, scratch.size());
    hex.MessageEnd();

    return QString::fromStdString(iv);
}

QString encrypt(const QString &plain, const QString &key, const QString &iv)
{
    byte decodedKey[AES::DEFAULT_KEYLENGTH];
    memset(decodedKey, 0x00, AES::DEFAULT_KEYLENGTH);
    StringSource(key.toStdString(), true, new HexDecoder(new ArraySink(decodedKey, AES::DEFAULT_KEYLENGTH)));

    byte decodedIv[AES::BLOCKSIZE];
    memset(decodedIv, 0x00, AES::BLOCKSIZE);
    StringSource(iv.toStdString(), true, new HexDecoder(new ArraySink(decodedIv, AES::BLOCKSIZE)));

    CFB_Mode<AES>::Encryption encryptor(decodedKey, AES::DEFAULT_KEYLENGTH, decodedIv);

    string chiper;
    StringSource(plain.toStdString(), true, new StreamTransformationFilter(encryptor, new HexEncoder(new StringSink(chiper))));

    return QString::fromStdString(chiper);
}

QString decrypt(const QString &chiper, const QString &key, const QString &iv)
{
    byte decodedKey[AES::DEFAULT_KEYLENGTH];
    memset(decodedKey, 0x00, AES::DEFAULT_KEYLENGTH);
    StringSource(key.toStdString(), true, new HexDecoder(new ArraySink(decodedKey, AES::DEFAULT_KEYLENGTH)));

    byte decodedIv[AES::BLOCKSIZE];
    memset(decodedIv, 0x00, AES::BLOCKSIZE);
    StringSource(iv.toStdString(), true, new HexDecoder(new ArraySink(decodedIv, AES::BLOCKSIZE)));

    CFB_Mode<AES>::Decryption decryptor(decodedKey, AES::DEFAULT_KEYLENGTH, decodedIv);

    string plain;
    StringSource(chiper.toStdString(), true, new HexDecoder(new StreamTransformationFilter(decryptor, new StringSink(plain))));

    return QString::fromStdString(plain);
}
