#ifndef KEYINFO_H
#define KEYINFO_H

#include <QString>

struct KeyInfo
{
    QString salt;
    int iterations;
    QString testChiper;
    QString testIv;

    KeyInfo() = delete;

    KeyInfo(const QString &salt, int iterations, const QString &testChiper, QString &testIv)
        : salt(salt), iterations(iterations), testChiper(testChiper), testIv(testIv)
    {
    }
};

#endif // KEYINFO_H
