#include "itemdialog.h"
#include <QFormLayout>
#include <QLabel>
#include <QDialogButtonBox>

ItemDialog::ItemDialog(QWidget *parent) : QDialog(parent), m_itemId(QUuid::createUuid())
{
    setUpUi();
}

void ItemDialog::setItem(const Item &item)
{
    m_nameInput->setText(item.name);
    m_loginInput->setText(item.login);
    m_passwordInput->setText(item.password);
    m_itemId = item.id;
}

void ItemDialog::save()
{
    Item item(m_itemId, m_nameInput->text().trimmed(), m_loginInput->text().trimmed(), m_passwordInput->text());
    if (item.name.size() > 0 && item.login.size() > 0 && item.password.size() > 0)
    {
        accept();
        emit saved(item);
    }
}

void ItemDialog::setUpUi()
{
    auto formLayout = new QFormLayout();

    m_nameInput = new QLineEdit();
    m_loginInput = new QLineEdit();
    m_passwordInput = new QLineEdit();
    m_passwordInput->setEchoMode(QLineEdit::PasswordEchoOnEdit);

    auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    auto nameLabel = new QLabel("&Name:");
    nameLabel->setBuddy(m_nameInput);
    auto loginLabel = new QLabel("&Login:");
    loginLabel->setBuddy(m_loginInput);
    auto passwordLabel = new QLabel("&Password:");
    passwordLabel->setBuddy(m_passwordInput);

    formLayout->addRow(nameLabel, m_nameInput);
    formLayout->addRow(loginLabel, m_loginInput);
    formLayout->addRow(passwordLabel, m_passwordInput);
    formLayout->addRow(buttonBox);
    formLayout->setSizeConstraint(QLayout::SetFixedSize);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &ItemDialog::save);

    setLayout(formLayout);
}
