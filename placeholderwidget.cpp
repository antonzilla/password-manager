#include "placeholderwidget.h"
#include <QPainter>

PlaceholderWidget::PlaceholderWidget(const QString &placeholder, QWidget *parent)
    : QWidget(parent), m_placeholder(placeholder)
{
}

void PlaceholderWidget::setPlaceholderText(const QString &text)
{
    m_placeholder = text.trimmed();
}

void PlaceholderWidget::paintEvent(QPaintEvent *e)
{
    QWidget::paintEvent(e);
    if (m_placeholder.size() > 0)
    {
        QPainter p(this);
        p.drawText(rect(), Qt::AlignCenter, m_placeholder);
    }
}
