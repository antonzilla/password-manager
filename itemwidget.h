#ifndef ITEMWIDGET_H
#define ITEMWIDGET_H

#include "item.h"
#include <QUuid>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>

class ItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ItemWidget(QWidget *parent = nullptr);
    Item getItem() const;

public slots:
    void setItem(const Item &item);
    void clear();

signals:
    void updated(Item item);
    void deleted();
    void passwordCopied();
    void loginCopied();

private slots:
    void editItem();
    void deleteItem();
    void copyPassword();
    void copyLogin();

private:
    QLabel *m_nameLabel;
    QLineEdit *m_loginInput;
    QLineEdit *m_passwordInput;
    QUuid m_itemId;

    void setUpUi();
};

#endif // ITEMWIDGET_H
